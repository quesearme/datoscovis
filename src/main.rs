use std::env;

use csvsc::prelude::*;
use encoding::all::UTF_8;

/**
"FECHA_ACTUALIZACION","ID_REGISTRO","ORIGEN","SECTOR","ENTIDAD_UM","SEXO","ENTIDAD_RES","MUNICIPIO_RES","TIPO_PACIENTE","FECHA_INGRESO","FECHA_SINTOMAS","FECHA_DEF","INTUBADO","NEUMONIA","EDAD","NACIONALIDAD","EMBARAZO","HABLA_LENGUA_INDIG","INDIGENA",

Estado?
"ENTIDAD_NAC",

Factores de riesgo
"DIABETES","EPOC","ASMA","INMUSUPR","HIPERTENSION","OTRA_COM","CARDIOVASCULAR","OBESIDAD","RENAL_CRONICA","TABAQUISMO",

"OTRO_CASO","TOMA_MUESTRA","RESULTADO_LAB","CLASIFICACION_FINAL","MIGRANTE","PAIS_NACIONALIDAD","PAIS_ORIGEN","UCI"
*/

fn main() {
    let args: Vec<_> = env::args().collect();

    let filename = args.get(1).expect("Need a file argument");

    let mut chain = InputStream::from_paths(
            vec![filename], UTF_8,
        ).unwrap()

        .select(vec![
            // datos básicos
            "SEXO", "EDAD",

            // ubicación
            "ENTIDAD_RES", "MUNICIPIO_RES",

            // factores de riesgo
            "DIABETES","EPOC","ASMA","INMUSUPR","HIPERTENSION","OTRA_COM","CARDIOVASCULAR","OBESIDAD","RENAL_CRONICA","TABAQUISMO",
        ])

        .reduce(vec![
            Reducer::with_name("count").count(),
        ]).unwrap()

        .flush(Target::path("output/datos.csv")).unwrap()

        .into_iter();

    while let Some(row) = chain.next() {
        if let Err(e) = row {
            eprint!("{}", e);
        }
    }
}
