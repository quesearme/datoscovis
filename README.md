# Procesamiento de datos COVID usando CSVSC

[Fuente de datos](https://www.gob.mx/salud/documentos/datos-abiertos-152127)

## ¿Cómo se usa?

Necesitas [el lenguaje de programación rust](https://rustup.rs)

Obtener los datos de la fuente y ponerlos en alguna carpeta, digamos
`data/datos.csv`, y luego hacer

    cargo run --release -- datos/data.csv

para obtener algunos resultados.
