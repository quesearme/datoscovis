# Descriptores

Descripcciones de cada columna de los datos proporcionados por el gobierno.

## FECHA_ACTUALIZACION

La base de datos se alimenta diariamente, esta variable permite identificar la fecha de la ultima actualizacion.	AAAA-MM-DD

## ID_REGISTRO

Número identificador del caso	TEXTO

## ORIGEN

La vigilancia centinela se realiza a través del sistema de unidades de salud monitoras de enfermedades respiratorias (USMER). Las USMER incluyen unidades médicas del primer, segundo o tercer nivel de atención y también participan como USMER las unidades de tercer nivel que por sus características contribuyen a ampliar el panorama de información epidemiológica, entre ellas las que cuenten con especialidad de neumología, infectología o pediatría. (Categorías en Catalógo Anexo).	CATÁLOGO: ORIGEN

## SECTOR

Identifica el tipo de institución del Sistema Nacional de Salud que brindó la atención.	CATÁLOGO: SECTOR

## ENTIDAD_UM

Identifica la entidad donde se ubica la unidad medica que brindó la atención.	CATALÓGO: ENTIDADES

## SEXO

Identifica al sexo del paciente.	CATÁLOGO: SEXO

## ENTIDAD_NAC

Identifica la entidad de nacimiento del paciente.	CATALÓGO: ENTIDADES

## ENTIDAD_RES

Identifica la entidad de residencia del paciente.	CATALÓGO: ENTIDADES

## MUNICIPIO_RES

Identifica el municipio de residencia del paciente.	CATALÓGO: MUNICIPIOS

## TIPO_PACIENTE

Identifica el tipo de atención que recibió el paciente en la unidad. Se denomina como ambulatorio si regresó a su casa o se denomina como hospitalizado si fue ingresado a hospitalización.	CATÁLOGO: TIPO_PACIENTE

## FECHA_INGRESO

Identifica la fecha de ingreso del paciente a la unidad de atención.	AAAA-MM-DD

## FECHA_SINTOMAS

Idenitifica la fecha en que inició la sintomatología del paciente.	AAAA-MM-DD

## FECHA_DEF

Identifica la fecha en que el paciente falleció.	AAAA-MM-DD

## INTUBADO

Identifica si el paciente requirió de intubación.	CATÁLOGO: SI_ NO

## NEUMONIA

Identifica si al paciente se le diagnosticó con neumonía.	CATÁLOGO: SI_ NO

## EDAD

Identifica la edad del paciente.	NÚMERICA EN AÑOS

## NACIONALIDAD

Identifica si el paciente es mexicano o extranjero.	CATÁLOGO: NACIONALIDAD

## EMBARAZO

Identifica si la paciente está embarazada.	CATÁLOGO: SI_ NO

## HABLA_LENGUA_INDIG

Identifica si el paciente habla lengua índigena.	CATÁLOGO: SI_ NO

## INDIGENA

Identifica si el paciente se autoidentifica como una persona indígena. 	CATÁLOGO: SI_ NO

## DIABETES

Identifica si el paciente tiene un diagnóstico de diabetes. 	CATÁLOGO: SI_ NO

## EPOC

Identifica si el paciente tiene un diagnóstico de EPOC. 	CATÁLOGO: SI_ NO

## ASMA

Identifica si el paciente tiene un diagnóstico de asma. 	CATÁLOGO: SI_ NO

## INMUSUPR

Identifica si el paciente presenta inmunosupresión.	CATÁLOGO: SI_ NO

## HIPERTENSION

Identifica si el paciente tiene un diagnóstico de hipertensión. 	CATÁLOGO: SI_ NO

## OTRAS_COM

Identifica si el paciente tiene diagnóstico de otras enfermedades.	CATÁLOGO: SI_ NO

## CARDIOVASCULAR

Identifica si el paciente tiene un diagnóstico de enfermedades cardiovasculares. 	CATÁLOGO: SI_ NO

## OBESIDAD

Identifica si el paciente tiene diagnóstico de obesidad.	CATÁLOGO: SI_ NO

## RENAL_CRONICA

Identifica si el paciente tiene diagnóstico de insuficiencia renal crónica.	CATÁLOGO: SI_ NO

## TABAQUISMO

Identifica si el paciente tiene hábito de tabaquismo.	CATÁLOGO: SI_ NO

## OTRO_CASO

Identifica si el paciente tuvo contacto con algún otro caso diagnósticado con SARS CoV-2	CATÁLOGO: SI_ NO

## TOMA_MUESTRA_LAB

Identifica si al paciente se le tomó muestra de laboratorio.	CATÁLOGO: SI_ NO

## RESULTADO_LAB

Identifica el resultado del análisis de la muestra reportado por el  laboratorio de la Red Nacional de Laboratorios de Vigilancia Epidemiológica (INDRE, LESP y LAVE) y laboratorios privados avalados por el InDRE cuyos resultados son registrados en SISVER. (Catálogo de resultados diagnósticos anexo).	CATÁLOGO: RESULTADO_LAB

## TOMA_MUESTRA_ANTIGENO

Identifica si al paciente se le tomó muestra de antígeno para SARS-CoV-2	CATÁLOGO: SI_ NO

## RESULTADO_ANTIGENO

Identifica el resultado del análisis de la muestra de antígeno tomada al paciente	CATÁLOGO: RESULTADO_ANTIGENO

## CLASIFICACION_FINAL

Identifica si el paciente es un caso de COVID-19 según el catálogo "CLASIFICACION_FINAL".	CATÁLOGO: CLASIFICACION_FINAL

## MIGRANTE

Identifica si el paciente es una persona migrante.	CATÁLOGO: SI_ NO

## PAIS_NACIONALIDAD

Identifica la nacionalidad del paciente.	TEXTO, 99= SE IGNORA

## PAIS_ORIGEN

Identifica el país del que partió el paciente rumbo a México.	TEXTO, 97= NO APLICA

## UCI

Identifica si el paciente requirió ingresar a una Unidad de Cuidados Intensivos.	CATÁLOGO: SI_NO
