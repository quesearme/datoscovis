## Referencias Bibliográficas

En el archivo llamado **"referencias.bib"** contendrá las fuentes bibliográficas usadas para este proyecto; esta base de referencias puede ser consultada por cualquier software compatible con el uso de [Bibtex](https://es.wikipedia.org/wiki/BibTeX)<sup>([1](http://www.bibtex.org/))</sup>.
